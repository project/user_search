********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: User Search
Author: Robert Castelo <services at cortextcommunications dot com>
Drupal: 4.6.x

********************************************************************
DESCRIPTION:

Enables complex searches of user profiles.
Replaces 'Users' tab on search pages.


********************************************************************
INSTALLATION:

Note: It is assumed that you have Drupal up and running.  Be sure to
check the Drupal web site if you need assistance.  If you run into
problems, you should always read the INSTALL.txt that comes with the
Drupal package and read the online documentation.

1. Place the entire user_search directory into your Drupal modules/
   directory.

2. Enable the user_search module by navigating to:

     administer > modules
     
  Click the 'Save configuration' button at the bottom to commit your
  changes.
  
********************************************************************
CONFIGURATION:

1. Access control options
    Control what roles can use search.
    (administer -> access control)
    
    'search module'
      - 'search content'  
    
    'user module'
      - 'access user profiles'
      
2. Configure the search form and results display
    (administer -> settings -> user search)


********************************************************************
TO DO

* Date range searches
* Case sensitive/insensitive searches
* Watchdog integration
* First page of results - add keywords and operators to URL
   (for bookmarking)


